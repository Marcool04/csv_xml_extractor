#!/usr/bin/env perl

###############################################################################################
#
# CSV with MARC XML encoded data extractor
# Produces a pure CSV file, with -;- as a separator, all data quoted with -"-
# Written by Mark Collins (tera_1225[at]hotmail[dot]com)
# during an internship at CREDO, Aix-Marseille University/CNRS/EHESS, Marseille, France
# Special thanks to Judith Hannoun
#
# References: 
#    https://wiki.koha-community.org/wiki/SQL_Reports_Library
#    https://en.wikipedia.org/wiki/Comma-separated_values
#    https://en.wikipedia.org/wiki/MARC_standards#MARCXML
#   
###############################################################################################


use strict;
use warnings;

use open ':encoding(utf8)';

use Getopt::Long;
use Hash::Ordered;
use List::MoreUtils qw(uniq);
use XML::Simple;


# Arguments and checks
my $input_file;
my $output_file;
my $verbose;
my $valid_options = GetOptions ("input=s" => \$input_file,
  "output=s"   => \$output_file,
  "help|?"  => sub {usage(); exit 0}
);
if (not $valid_options) {
  usage();
  exit -2;
}

sub usage {
  print "This is a script to convert a CSV file with a MARC MXL entry into pure CSV.\n";
  print "Usage: $0 -input input_file.csv -output output_file.csv [-help|-?]\n";
  print "   -input input_file.csv -> File to read CSV with MARC XML from\n";
  print "   -output output_file.csv -> File to write output to\n";
}

if ( ! -s $input_file ) {
  print "Input file not found or empty.\n";
  exit -1;
}
if ( -s $output_file ) {
  print "WARNING: output file $output_file exists. Type \"yes\" to overwrite.\n";
  while (<STDIN>) {
    chomp $_;
    if ($_ ne "yes") {
      print "Canceled.\n";
      exit -1;
    } else {
      last;
    }
  }
}


# Open files
open( my $input, '<', $input_file )
  or die ("Failed to open input file $input_file: $!");
open( my $output, '>>', $output_file )
  or die ("Failed to open output file $output_file: $!");

# Count lines in input file
for (<$input>) {};
my $line_total=$.;
close $input;

my $lines_in = 0;
my $lines_out = 0;
my $xml_started = 0;
my $current_xml = '';
# This needs to be created here in order for the lables to be available on line 0
my @blank_element = (
  editor_210 => '',
  abstract_330 => '',
  table_327 => '',
  table_359 => '',
  keywords_606_main => '',
  keywords_606_607_geo => '',
  main_author_700 => '',
  other_authors_702 => '',
);
my $processed_xml = Hash::Ordered->new(@blank_element);

# Reopen file as count will have set file descriptor to last line
open( $input, '<', $input_file )
  or die ("Failed to open input file $input_file: $!");


####################################################################
# MAIN LOOP
# Read input line by line
while (<$input>) {
  print "\r"x100;
  print "Line: ${lines_in}/${line_total}";

  my $line = $_;
  chomp($line);
  $lines_in++;
  
  # START XML
  if (index($line, ';"<?xml') > 0) {
    $xml_started = 1;
    my @split_line = split(/;"<\?xml/, $line);
    my $xml_filtered_line = $split_line[0];
    my $xml_fragment = $split_line[1];
    $current_xml .= '<?xml' . $xml_fragment . "\n";
    print $output $xml_filtered_line . ';';
    $lines_out++;
    next;
  }

  # MIDDLE XML LINE
  # There is no "closing" xml tag, but there happens to always be a line
  # containing simply a double quote, so we test for that
  if ($xml_started && $line ne '"') {
    $current_xml .= $line . "\n";
    next;
  }

  # END XML
  if ($xml_started && $line eq '"') {
    $xml_started = 0;
    process_xml($current_xml);
    # Here the map allows us to account for empty elements
    # Note the quotes around the whole thing
    print $output join(';', 
      map { '"'.(${processed_xml}->get($_) ? ${processed_xml}->get($_) : '').'"' } 
        ($processed_xml->keys) 
    ); 
    print $output "\n";
    $lines_out++;
    # Reset ordered hash, needs ->merge() method, not ->set()!!
    $processed_xml->merge(@blank_element);
    $current_xml = '';
    next;
  }

  # Headings
  if (index($line, "marcxml") >= 0) {
    my $new_column_names = join(';', $processed_xml->keys);
    $line =~ s/marcxml/$new_column_names/;
    print $output $line . "\n";
    $lines_out++;
    next;
  }

  # Lines with no xml
  # Probably never actually called
  print $output $line . join(';', $processed_xml->values) . "\n";
  $lines_out++;
}

print "\nDone. Lines in: $lines_in, lines out: $lines_out.";
exit 0;


sub process_xml {
  # This function doesn't return anything
  # it sets an overarching ordered hash: $processed_xml

  my $xml = shift;

  my $unquoted_xml = dtsq($xml);

  my $xml_hash_reference = XMLin( 
    $unquoted_xml,
    ForceArray => ['datafield', 'subfield'],
  );
  my %xml_hash = %$xml_hash_reference;
  my $datafields_ref = $xml_hash{datafield};
  $processed_xml->concat(
    'editor_210' => 
    (get_tag_code({ tag => 210, subfield_codes => ["c"] }, $datafields_ref ))[0]
  );
  $processed_xml->concat(
    'table_327' =>
    (get_tag_code({ tag => 327, subfield_codes => ["a"] }, $datafields_ref ))[0]
  );
  $processed_xml->concat(
    'abstract_330' =>
    (get_tag_code({ tag => 330, subfield_codes => ["a"] }, $datafields_ref ))[0]
  );
  $processed_xml->concat(
    'table_359' =>
    (get_tag_code({ tag => 359, subfield_codes => ["b", "c"] }, $datafields_ref ))[0]
  );
  $processed_xml->concat(
    'keywords_606_main' =>
    (get_tag_code({ tag => 606, subfield_codes => ["a"] }, $datafields_ref ))[0]
  );
  $processed_xml->concat(
    'keywords_606_607_geo' =>
    join( ",", uniq(
        get_tag_code({ tag => 606, subfield_codes => ["y"] }, $datafields_ref ),
        get_tag_code({ tag => 607, subfield_codes => ["a"] }, $datafields_ref ) 
      )
    )
  );
  $processed_xml->concat(
    'main_author_700' =>
    join ( " ",
     get_tag_code({tag => 700,subfield_codes => ["a", "b"]}, $datafields_ref)
    )
  );
  my $counter = 0;
  foreach my $author ( get_tag_code({tag => 702,subfield_codes => ["a", "b"]}, $datafields_ref) ) {
    if ( $author ne '') { 
      $processed_xml->concat(
        'other_authors_702' =>
        ( ($counter > 0) ? ( $counter %2 == 0 ? "," : " ") : '' ) . $author
        # ^ This ugly even/odd logic is for name/surname discrimination
        # so things are in Surname Name, Surname1 Name1, etc. format
      );
    }
    $counter++;
  }
  return;
}

sub stdq {
  #single to doubl quotes
  my $string = shift or return;
  $string =~ s/"/""/g;
  return $string;
}

sub dtsq {
  #double to single quotes
  my $string = shift or return;
  $string =~ s/""/"/g;
  return $string;
}

sub get_tag_code {
  my $params_ref = shift;
  my $datafields_ref = shift;
  my %params = %$params_ref;
  my @datafields = @$datafields_ref;
  
  my @results;

  foreach my $entry_ref (@datafields) {
    my %entry=%$entry_ref;
    unless ($entry{tag}) {
      next;
    }
    unless ($entry{tag} eq $params{tag} && $entry{subfield}) {
      next;
    }
    foreach my $subfields_ref ($entry{subfield}) {
      my @subfields = @{$subfields_ref};
      foreach my $subfield_ref (@subfields) {
        my @codes = @{$params{subfield_codes}};
        my %subfield = %$subfield_ref;
        foreach my $code (@codes) {
          if ($subfield{code} && ($subfield{code} eq $code) && $subfield{content} ) {
            push( @results, stdq($subfield{content}) );
          }
        }
      }
    }
  }
  my @unique_results = uniq(@results);
  return @unique_results;
}

1;
