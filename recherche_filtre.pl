#!/usr/bin/env perl

use strict;
use warnings;

my $counter = 0;
while (<>) {
  if (
    $_ =~ /raratonga/gi                                             ||
    $_ =~ /cook island[s]{0,1}/gi                                   ||
    $_ =~ /[îiÎ]les cook/gi                                         ||
    (
      !($_ =~ /James Cook/gi || $_ =~ /Cap(i|)tain(e|) Cook/gi)     &&
      (
        ($_ =~ /.*(cook\b).*([iîI]le[s]{0,1})|(?2).*(?1).*/gi)      ||
        ($_ =~ /.*(cook\b)[\s,;\(\)]+(island[s ])|(?2).*(?1).*/gi)
      )
    )      
  ) {
    print $_;
    $counter++;
  }
  print STDERR "\r"x100, "Lines in: $. lines out: $counter";
}
exit;
